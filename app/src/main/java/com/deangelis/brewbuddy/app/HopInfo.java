package com.deangelis.brewbuddy.app;

/**
 * Created by chrisdeangelis on 10/9/14.
 */
public class HopInfo {
    private String name;
    private String weight;
    private String time;

    public void setName(String name) {this.name = name;}

    public void setTime(String time){
        this.time = time;
    }

    public void setWeight(String weight){
        this.weight = weight;
    }

    public String getName(){
        return name;
    }
    public String getWeight() { return weight; }
    public String getTime(){ return time; }


}

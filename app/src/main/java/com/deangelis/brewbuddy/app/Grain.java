package com.deangelis.brewbuddy.app;

/**
 * Created by Chris on 8/30/2014.
 */
public class Grain {

    private String name;
    private int color,gravity;

    public Grain(){}

    public Grain(String name, int color, int gravity){
        super();
        this.name = name;
        this.color = color;
        this.gravity = gravity;
    }

    public String getName(){
        return this.name;
    }

    public int getColor(){
        return this.color;
    }

    public int getGravity(){
        return this.gravity;
    }
}

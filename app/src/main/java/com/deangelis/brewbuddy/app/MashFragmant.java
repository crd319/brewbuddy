package com.deangelis.brewbuddy.app;

import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;



/**
 * Created by Chris on 6/7/2014.
 *
 * CLASS: MashFragment
 *
 * Fragment to perform calculations for the Volume and Temperature of a Mash's Strike Water.
 * Calculates the mash efficiency based on gravity measurements.  Create a 1 hour timer object to
 * keep track of the duration of the Mash.
 */
public class MashFragmant extends Fragment {

    EditText grainWeight, ratio, targetTemp, grainTemp, expectedGravity, measuredGravity = null;
    View mashView;
    Button start, stop;
    TextView volumeText, strikeTempText, efficiency = null;
    TextView Timer;
    CountDownTimer MashTime;
    boolean isActive = false;
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MashFragmant newInstance(int sectionNumber) {
        MashFragmant fragment = new MashFragmant();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public MashFragmant() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mashView = inflater.inflate(R.layout.fragment_mash, container, false);

        textChangeMethodStrike();
        textChangeMethodEfficiency();
        Timer = (TextView) mashView.findViewById(R.id.mashTimer);
        start = (Button) mashView.findViewById(R.id.mashStart);
        stop = (Button) mashView.findViewById(R.id.mashReset);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MashTimer();
            }
        });

        stop.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(isActive) {
                    MashTime.cancel();
                    Timer.setText("01:00:00");
                    isActive = false;
                }
            }
        });

        return mashView;
    }

    //Listener method for Strike Water calculations

    public void textChangeMethodStrike(){

        grainWeight = (EditText) mashView.findViewById(R.id.grainWeightInput);
        ratio = (EditText) mashView.findViewById(R.id.ratio);
        volumeText = (TextView) mashView.findViewById(R.id.volumeText);

        targetTemp = (EditText) mashView.findViewById(R.id.targetTemp);
        grainTemp = (EditText) mashView.findViewById(R.id.grainTemp);
        strikeTempText = (TextView) mashView.findViewById(R.id.strikeTempText);

        TextWatcher strikeFilterTextWatcher = new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String sGrainWeight = grainWeight.getText().toString();
                String sRatio = ratio.getText().toString();
                String sTargetTemp = targetTemp.getText().toString();
                String sGrainTemp = grainTemp.getText().toString();
                if( !(sGrainWeight.equals("")) && !(sRatio.equals(""))){
                    float fGrainWeight = Float.parseFloat(sGrainWeight);
                    float fRatio = Float.parseFloat(sRatio);
                    double calcVolume = (fGrainWeight * fRatio) / 4.0;
                    volumeText.setText(String.format("%.2f", calcVolume)+" gal");
                }

                if( !(sTargetTemp.equals("")) && !(sGrainTemp.equals("")) && !(sRatio.equals(""))){
                    float fRatio = Float.parseFloat(sRatio);
                    float fTargetTemp = Float.parseFloat(sTargetTemp);
                    float fGrainTemp = Float.parseFloat(sGrainTemp);
                    double calcTemp = (0.2/fRatio)*(fTargetTemp-fGrainTemp)+fTargetTemp;
                    strikeTempText.setText(String.format("%.2f", calcTemp) +" F");
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        };

            //Attach the TextWatcher object to each of the EditText fields
            grainWeight.addTextChangedListener(strikeFilterTextWatcher);
            ratio.addTextChangedListener(strikeFilterTextWatcher);
            targetTemp.addTextChangedListener(strikeFilterTextWatcher);
            grainTemp.addTextChangedListener(strikeFilterTextWatcher);
    }

    //Listener for the Efficiency calculation EditText fields

    public void textChangeMethodEfficiency(){
        expectedGravity = (EditText) mashView.findViewById(R.id.expectedGravityInput);
        measuredGravity = (EditText) mashView.findViewById(R.id.measuredGravityInput);
        efficiency= (TextView) mashView.findViewById(R.id.efficiencyText);

        TextWatcher effFilterTextWatcher = new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String sExpectedGravity = expectedGravity.getText().toString();
                String sMeasuredGravity = measuredGravity.getText().toString();

                if( !(sExpectedGravity.equals("")) && !(sMeasuredGravity.equals(""))){
                    float fMeasuredGravity = Float.parseFloat(sMeasuredGravity);
                    float fExpectedGravity = Float.parseFloat(sExpectedGravity);
                    double calcEff = (((1-fMeasuredGravity)*1000)/((1-fExpectedGravity)*1000))*100;
                    efficiency.setText(String.format("%.2f", calcEff)+" %");
                }
            }

            @Override
            public void afterTextChanged(Editable arg0) {
            }
        };

        //Attach the TextWatcher object to each of the EditText fields
        expectedGravity.addTextChangedListener(effFilterTextWatcher);
        measuredGravity.addTextChangedListener(effFilterTextWatcher);
    }

    //Method to create and execute the Mash CountDownTimer object
    public void MashTimer() {
        if (!isActive) {
            isActive = true;
            String sTime = Timer.getText().toString();
            String Hours = sTime.substring(0, 2);
            String Minutes = sTime.substring(3, 5);
            String Seconds = sTime.substring(6);
            Long iHours = Long.parseLong(Hours);
            Long iMin = Long.parseLong(Minutes);
            Long iSec = Long.parseLong(Seconds);

            //Convert hours, minutes, seconds to millisections
            long totalTime = (iHours * 3600 + iMin * 60 + iSec) * 1000;

            MashTime = new CountDownTimer(totalTime, 1000) {
                @Override
                public void onTick(long timeToComplete) {

                    //Convert milliseconds to seconds, minutes, hour components.
                    int seconds = (int) (timeToComplete / 1000) % 60;
                    int minutes = (int) ((timeToComplete / (1000 * 60)) % 60);
                    int hours = (int) ((timeToComplete / (1000 * 60 * 60)) % 24);

                    String sHours = String.format("%02d", hours);
                    String sSec = String.format("%02d", seconds);
                    String sMin = String.format("%02d", minutes);

                    Timer.setText(sHours + ":" + sMin + ":" + sSec);
                }

                @Override
                public void onFinish() {
                    Timer.setText("00:00:00");
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                    Ringtone r = RingtoneManager.getRingtone(getActivity().getApplicationContext(), notification);
                    r.play();
                    isActive = false;
                }
            };
            MashTime.start();
        }
    }
}
package com.deangelis.brewbuddy.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import java.util.ArrayList;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Chris on 7/26/2014.
 *
 * CLASS: CustomAlarmAdapter
 *
 * Custom extension of a standard ArrayAdapter for an AlarmInfo object.  Also contains a button
 * to remove a row.
 */
public class CustomAlarmAdapter extends ArrayAdapter<AlarmInfo>{
    private Context appContext=null;
    private ArrayList<AlarmInfo> items = null;

    public CustomAlarmAdapter(Context context, int textViewResourceId, ArrayList<AlarmInfo> items){
        super(context, textViewResourceId, items);
        this.appContext = context;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.list_items, null);
        }
        AlarmInfo ai = items.get(position);
        if (ai != null) {
            //Get value fields from the View
            TextView timeText = (TextView) v.findViewById(R.id.alarmTime);
            TextView reasonText = (TextView) v.findViewById(R.id.alarmReason);
            Button btnDelete = (Button) v.findViewById(R.id.lst_item_Delete);
            btnDelete.setTag(position);

            //When the delete button is pressed
            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Get the current position
                    String pos = view.getTag().toString();
                    int remPos = Integer.parseInt(pos);
                    //Remove the position and notify the View that a change has occured
                    items.remove(remPos);
                    notifyDataSetChanged();
                }
            });

            if (timeText != null){
                timeText.setText(ai.getTime());
            }
            if (reasonText != null){
                reasonText.setText(ai.getReason());
            }
        }
        return v;
    }
}

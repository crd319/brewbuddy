package com.deangelis.brewbuddy.app;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Chris on 8/30/2014.
 */
public class GrainDatabaseBuilder extends SQLiteOpenHelper{

    private static final int DATABASE_VERSION=1;
    //Database Name
    private static final String DATABASE_NAME= "GrainProperties.db";
    //Table Name
    private static final String TABLE_GRAINS = "grains";

    //Column names
    private static final String KEY_NAME = "name";
    private static final String KEY_COLOR = "color";
    private static final String KEY_GRAVITY = "gravity";


    public GrainDatabaseBuilder(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_GRAIN_TABLE = "CREATE TABLE "+TABLE_GRAINS+"("+
                KEY_NAME+" TEXT PRIMARY KEY, "+
                KEY_COLOR+" INTEGER, "+
                KEY_GRAVITY+" INTEGER)";
        sqLiteDatabase.execSQL(CREATE_GRAIN_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS grains");
        this.onCreate(sqLiteDatabase);
    }

    private static final String[] COLUMNS ={KEY_NAME, KEY_COLOR, KEY_GRAVITY};

    public void addGrain(Grain newGrain){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_NAME, newGrain.getName());
        values.put(KEY_COLOR, newGrain.getColor());
        values.put(KEY_GRAVITY, newGrain.getGravity());

        db.insert(TABLE_GRAINS, null, values);
        db.close();
    }

    /*
     * Method: getGrains
     *
     * VARIABLES: Name - the name of the grain to be Queried
     *
     * RETURN: Grain - The Grain object to be returned
     *
     */
    public Grain getGrain(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(TABLE_GRAINS, COLUMNS, name, null, null, null, null);
        Grain grain = new Grain(cursor.getString(0), +
                Integer.parseInt(cursor.getString(1)),+
                Integer.parseInt(cursor.getString(2)));
        return grain;
    }

    public List<Grain> getAllGrains(){
        List<Grain> allGrains = new LinkedList<Grain>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM "+TABLE_GRAINS, null);

        Grain g = new Grain();
                if (c.moveToFirst()){
                    do{
                        Grain grain = new Grain(c.getString(0), +
                                Integer.parseInt(c.getString(1)),+
                                Integer.parseInt(c.getString(2)));
                        allGrains.add(grain);
                    }while(c.moveToNext());
                }

        return allGrains;
    }
}

package com.deangelis.brewbuddy.app;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Chris on 8/2/2014.
 *
 * CLASS: Notification
 *
 * Creates a new notification object
 */
public class Notification extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notification);
    }
}

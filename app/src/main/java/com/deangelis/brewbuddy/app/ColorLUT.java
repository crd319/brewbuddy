package com.deangelis.brewbuddy.app;

import java.util.HashMap;

/**
 * Created by chrisdeangelis on 10/9/14.
 * 
 * CLASS: ColorLUT
 * 
 * Applies a SRM to RGB look up table to get an
 * approximate color 
 */
public class ColorLUT {

    private HashMap<Float, int[]> LUT;

    public ColorLUT(){
        LUT = new HashMap<Float, int[]>();
        LUT.put(new Float(0.1), new int[]{250, 250, 210});
        LUT.put(new Float(0.2), new int[]{250, 250, 204});
        LUT.put(new Float(0.3), new int[]{250, 250, 199});
        LUT.put(new Float(0.4), new int[]{250, 250, 193});
    }

    public int[] getColor(float srmColor){
        return LUT.get(srmColor);
    }

}

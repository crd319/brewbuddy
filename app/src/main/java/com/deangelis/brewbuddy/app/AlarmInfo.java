package com.deangelis.brewbuddy.app;

/**
 * Created by Chris on 7/26/2014.
 *
 * CLASS: AlarmInfo
 * Basic class to contain information of a created alarm
 */
public class AlarmInfo {
    private String time;
    private String reason;

    public void setTime(String t){
        time = t;
    }

    public void setReason(String r){
        reason = r;
    }

    public String getTime(){
        return time;
    }

    public String getReason(){
        return reason;
    }
}

package com.deangelis.brewbuddy.app;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.NumberPicker;
import android.widget.Spinner;

/**
 * Created by Chris on 8/1/2014.
 *
 * CLASS: AlarmSelectorPopup
 *
 * Class to create and display a DialogFragment for the user to add an alarm notification
 * to the BoilFragment
 */
public class AlarmSelectorPopup extends DialogFragment {
    NumberPicker alarmTimePicker;
    Spinner reasonList;
    int timeVal = 1;
    int timeReturn;
    String reasonReturn;

    //Overloaded newInstance constructor method
    static AlarmSelectorPopup newInstance(int time){
        AlarmSelectorPopup f = new AlarmSelectorPopup();
        Bundle args = new Bundle();
        args.putInt("num", time);
        f.setArguments(args);

        return f;
    }

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        timeVal = getArguments().getInt("num");
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        //Get the View from the layout file
        View v=inflater.inflate(R.layout.alarm_popup, null);

        reasonList = (Spinner) v.findViewById(R.id.reasonSpinner);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.alarm_reason_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        reasonList.setAdapter(adapter);

        alarmTimePicker = (NumberPicker) v.findViewById(R.id.alarmTimePickerSpinner);

        alarmTimePicker.setMaxValue(timeVal);
        alarmTimePicker.setMinValue(0);

        builder.setView(v)
                //Create a "Submit" button to send values to the parent Fragment
                .setPositiveButton(R.string.submitButtonStr,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                timeReturn = alarmTimePicker.getValue();
                                reasonReturn = reasonList.getSelectedItem().toString();
                                sendResult(102);
                                dismiss();
                            }
                        }
                )
                //Create a "Cancal" button to dismiss the DialogFragment without sending data
                .setNegativeButton(R.string.cancelButtonStr,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dismiss();
                            }
                        }
                );
                //return the created DialogFragment
                return builder.create();
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    //Method to send the user entered values to the parent Fragment as an Intent object
    public void sendResult(int CODE){
        Intent i = new Intent();
        i.putExtra("time_res", String.valueOf(timeReturn));
        i.putExtra("reason_res", String.valueOf(reasonReturn));
        getTargetFragment().onActivityResult(getTargetRequestCode(), CODE, i);
    }
}

package com.deangelis.brewbuddy.app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Chris on 6/7/2014.
 *
 * CLASS: GravityFragment
 *
 * Main Fragment for performing ABV calculations
 */
public class GravityFragment extends Fragment {

    EditText initalG = null;
    EditText finalG = null;
    TextView abv = null;
    View rootView;

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static GravityFragment newInstance(int sectionNumber) {
        GravityFragment fragment = new GravityFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public GravityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_gravity, container, false);

        textChangeMethod();
        return rootView;
    }

    public void textChangeMethod(){

        initalG = (EditText) rootView.findViewById(R.id.initalGravityInput);
        finalG = (EditText) rootView.findViewById(R.id.finalGravityInput);
        abv = (TextView) rootView.findViewById(R.id.abvText);
        //Create new TextWatcher object
        TextWatcher filterTextWatcher = new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count,int after)
            {
            }

            public void onTextChanged(CharSequence s,int start, int before,int count)
            {
                String sInitial = initalG.getText().toString();
                String sFinal = finalG.getText().toString();
                //When both fields are not empty
                if( !(sInitial.equals("")) && !(sFinal.equals(""))){
                    //Get the field contents
                    float fInital = Float.parseFloat(sInitial);
                    float fFinal = Float.parseFloat(sFinal);

                    //Calculate the ABV from the user defined values
                    double calcAbv = (fInital - fFinal) * 131.25;
                    abv.setText(String.format("%.2f", calcAbv)+" %");
                }
                else{
                    abv.setText("");
                }
            }

            @Override
            public void afterTextChanged(Editable arg0)
            {
            }
        };

        //Add listeners to the EditText fields
        initalG.addTextChangedListener(filterTextWatcher);
        finalG.addTextChangedListener(filterTextWatcher);
    }
}
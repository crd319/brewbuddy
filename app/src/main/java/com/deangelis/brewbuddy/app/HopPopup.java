package com.deangelis.brewbuddy.app;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

/**
 * Created by chrisdeangelis on 10/9/14.
 */
public class HopPopup extends DialogFragment {

    Spinner hopList;
    EditText time, weight;
    String returnHop, returnWeight, returnTime;

    //Overloaded newInstance constructor method
    static HopPopup newInstance(){
        HopPopup f = new HopPopup();
        Bundle args = new Bundle();

        return f;
    }

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        //Get the View from the layout file
        View v=inflater.inflate(R.layout.hop_addition_layout, null);

        hopList = (Spinner) v.findViewById(R.id.hopAddition);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.types_of_hops, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        hopList.setAdapter(adapter);

        time = (EditText) v.findViewById(R.id.hopTime);
        weight = (EditText) v.findViewById(R.id.hopWeight);

        builder.setView(v)
                //Create a "Submit" button to send values to the parent Fragment
                .setPositiveButton(R.string.submitButtonStr,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                returnHop = hopList.getSelectedItem().toString();
                                returnTime = (time.getText().toString());
                                returnWeight = (weight.getText().toString());
                                sendResult(201);
                                dismiss();
                            }
                        }
                )
                        //Create a "Cancal" button to dismiss the DialogFragment without sending data
                .setNegativeButton(R.string.cancelButtonStr,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dismiss();
                            }
                        }
                );
        //return the created DialogFragment
        return builder.create();
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    //Method to send the user entered values to the parent Fragment as an Intent object
    public void sendResult(int CODE){
        Intent i = new Intent();
        i.putExtra("hop_res", String.valueOf(returnHop));
        i.putExtra("time_res", String.valueOf(returnTime));
        i.putExtra("weight_res", String.valueOf(returnWeight));
        getTargetFragment().onActivityResult(getTargetRequestCode(), CODE, i);
    }
}

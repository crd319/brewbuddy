package com.deangelis.brewbuddy.app;

import android.app.NotificationManager;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Chris on 6/7/2014.
 *
 * CLASS: BoilFragment
 *
 * Fragment to create a timer, based on the user's input, with a variable number of alarms
 */
public class BoilFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */

    TextView Time = null;
    View rootView;
    String Hours, Minutes, Seconds;
    String sTime;
    Button start, set, stop, add;
    long iHours, iMin, iSec;
    boolean isActive = false;
    CountDownTimer BoilTime;
    ListView list;
    CustomAlarmAdapter alarmAdapter = null;
    ArrayList<AlarmInfo> newList = null;
    ArrayList<Long> allAlarms = new ArrayList<Long>();
    ArrayList<String> allReasons = new ArrayList<String>();
    private NotificationManager mNotificationManager;

    public static final int RESULT_OK = 101;
    public static final int RESULT_ALARM = 102;
    private static final String ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static BoilFragment newInstance(int sectionNumber) {
        BoilFragment fragment = new BoilFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public BoilFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_boil, container, false);
        newList = new ArrayList<AlarmInfo>();
        alarmAdapter = new CustomAlarmAdapter(getActivity(), R.layout.list_items, newList);
        Time = (TextView) rootView.findViewById(R.id.Time);
        list = (ListView) rootView.findViewById(R.id.alarmView);
        list.setAdapter(alarmAdapter);

        add = (Button) rootView.findViewById(R.id.addAlarmButton);

        //Create a button to call the popup window to add an alarm object
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!Time.getText().equals("00:00:00") && !isActive ) {
                    showAlarmDialog();
                }
            }
        });

        //Create a button to create and execute the CountDownTimer
        start = (Button) rootView.findViewById(R.id.startButton);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Timer();
            }
        });

        //Create a button to call the popup window to set the timer
        set = (Button) rootView.findViewById(R.id.setTimer);
        set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!isActive) {
                    showDialog();
                }
            }
        });

        //Creata button to stop the Timer and zero out the timer
        stop = (Button) rootView.findViewById(R.id.StopButton);
        stop.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(isActive) {
                    BoilTime.cancel();
                    Time.setText("00:00:00");
                    isActive = false;
                }
            }
        });
        return rootView;
    }

    //Method to create and execute a timer
    public void Timer() {
        if (!isActive) {
            isActive = true;
            if(Time.getText().toString() != null) {
                sTime = Time.getText().toString();
            }
            Hours = sTime.substring(0, 2);
            Minutes = sTime.substring(3, 5);
            Seconds = sTime.substring(6);
            iHours = Long.parseLong(Hours);
            iMin = Long.parseLong(Minutes);
            iSec = Long.parseLong(Seconds);
            long totalTime = (iHours * 3600 + iMin * 60 + iSec) * 1000;

            //Get all alarms and store them into a Array to be checked on each Tick
            //of the CountDownTimer
            for(int i=0; i<list.getCount(); i++){
                View row = list.getAdapter().getView(i, null, null);
                TextView v1 = (TextView) row.findViewById(R.id.alarmTime);
                TextView v2 = (TextView) row.findViewById(R.id.alarmReason);
                allAlarms.add(Long.parseLong(v1.getText().toString()));
                allReasons.add(v2.getText().toString());
            }

            //Create new CountDownTimer object
            BoilTime = new CountDownTimer(totalTime, 1000) {

                //On each tick of the Timer
                public void onTick(long millisUntilFinished) {

                    int seconds = (int) (millisUntilFinished / 1000) % 60;
                    int minutes = (int) ((millisUntilFinished / (1000 * 60)) % 60);
                    int hours = (int) ((millisUntilFinished / (1000 * 60 * 60)) % 24);

                    int totalMin = minutes+(hours*60);

                    String sHours = String.format("%02d", hours);
                    String sSec = String.format("%02d", seconds);
                    String sMin = String.format("%02d", minutes);

                    //Update the timer TextView
                    Time.setText(sHours + ":" + sMin + ":" + sSec);

                    //Check all alarms
                    for (int i=0; i<allAlarms.size(); i++) {
                        //If an alarm needs to be performed
                        if (totalMin == allAlarms.get(i) && seconds == 0) {
                            //display the notification to the user
                            displayNotification(allReasons.get(i));
                        }
                    }
                }

                public void onFinish() {
                    Time.setText("00:00:00");
                    Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                    Ringtone r = RingtoneManager.getRingtone(getActivity().getApplicationContext(), notification);
                    r.play();
                    displayNotification("End of Boil");
                    isActive = false;
                }
            }.start();
        }
    }

    //Display notification method
    protected void displayNotification(String reasonStr){
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Intent resultIntent = new Intent(getActivity(), Notification.class);

        //Build a new notification
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getActivity())
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setContentTitle("Boil Notification")
                        .setContentText(reasonStr)
                        .setSound(notification)
                        .setAutoCancel(true);

        resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getActivity());
        stackBuilder.addParentStack(Notification.class);
        stackBuilder.addNextIntent(resultIntent);

        //int requestID = (int) System.currentTimeMillis();
        //PendingIntent resultPendingIntent = PendingIntent.getActivity(getActivity(), requestID, resultIntent, 0);
        //mBuilder.setContentIntent(resultPendingIntent);
        mNotificationManager =
                (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);

        //Send the built notification to the notification Manager
        mNotificationManager.notify(0, mBuilder.build());
    }

    //Show the Alarm Dialog Box
    public void showAlarmDialog(){
        String sTime = Time.getText().toString();
        Hours = sTime.substring(0, 2);
        Minutes = sTime.substring(3, 5);
        int intHours = Integer.parseInt(Hours);
        int intMin = Integer.parseInt(Minutes);

        int totalTime = (intHours * 60 + intMin);

        getActivity().getSupportFragmentManager();
        new AlarmSelectorPopup();
        AlarmSelectorPopup alarm = AlarmSelectorPopup.newInstance(totalTime);
        alarm.setTargetFragment(this, 102);
        alarm.show(getFragmentManager(), "fragmentDialog");
    }

    public void showDialog(){
        getActivity().getSupportFragmentManager();
        TimerPopup popup = new TimerPopup();
        popup.setTargetFragment(this, 101);
        popup.show(getFragmentManager(), "fragmentDialog");
    }

    //Retrieve Intent objects from the Popup Windows that can be called from this Fragment
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        //Retrieve data from the set time popup window
        if(requestCode == RESULT_OK){
            Hours = data.getStringExtra("hour_res");
            Minutes = data.getStringExtra("minute_res");

            String sHours = String.format("%02d", Long.parseLong(Hours));
            String sMin = String.format("%02d", Long.parseLong(Minutes));

            Time.setText(sHours + ":" + sMin + ":00");
        }
        //Retrieve data from the Add Alarm button
        if(requestCode == RESULT_ALARM){
            String alarmReturn = data.getStringExtra("time_res");
            String reasonReturn = data.getStringExtra("reason_res");
            newList = new ArrayList<AlarmInfo>();
            AlarmInfo newInfo = new AlarmInfo();
            newInfo.setTime(alarmReturn);
            newInfo.setReason(reasonReturn);
            newList.add(newInfo);

            //Add the newly created alarm to the CustomArrayAdapter
            if(newList != null && newList.size() >0){
                alarmAdapter.notifyDataSetChanged();
                alarmAdapter.add(newList.get(0));
            }
            //Notify the adapter that changes were made
            alarmAdapter.notifyDataSetChanged();
        }
    }
}
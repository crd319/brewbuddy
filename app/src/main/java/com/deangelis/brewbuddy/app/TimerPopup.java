package com.deangelis.brewbuddy.app;


import android.content.Context;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.NumberPicker;

/**
 * Created by Chris on 6/18/2014.
 *
 * CLASS: TimerPopup
 */
public class TimerPopup extends DialogFragment {
    Context context;
    NumberPicker hourPicker, minutePicker;
    Button submitButton, cancelButton;
    int hourString, minuteString;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                               Bundle savedInstanceState) {
        context = getActivity().getApplicationContext();

        //AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View v=inflater.inflate(R.layout.timer_popup, null);

        hourPicker = (NumberPicker) v.findViewById(R.id.pickHours);
        minutePicker = (NumberPicker) v.findViewById(R.id.pickMinutes);

        submitButton = (Button) v.findViewById(R.id.submitTimeButton);
        cancelButton = (Button) v.findViewById(R.id.cancelTimeButton);

        hourPicker.setMaxValue(1);
        hourPicker.setMinValue(0);
        minutePicker.setMaxValue(59);
        minutePicker.setMinValue(0);

        //On pressing the Cancel button, dismiss (close) the window without passing any values
        //back to the parent Fragment
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        //On pressing the Submit button, pass the values selected by the user back to the
        //parent fragment
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hourString= hourPicker.getValue();
                minuteString = minutePicker.getValue();
                sendResult(101);
                dismiss();
            }
        });
        return v;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
    }

    //Method to send the values back to the Boil Framgent by means of an Intent object
    //to store the data.
    public void sendResult(int CODE){
        Intent i = new Intent();
        i.putExtra("hour_res", String.valueOf(hourString));
        i.putExtra("minute_res", String.valueOf(minuteString));
        getTargetFragment().onActivityResult(getTargetRequestCode(), CODE, i);
    }
}
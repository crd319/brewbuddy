package com.deangelis.brewbuddy.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * Created by chrisdeangelis on 10/9/14.
 */
public class CustomHopArrayAdapter extends ArrayAdapter<HopInfo> {
    private Context appContext=null;
    private ArrayList<HopInfo> hopItems = null;

    public CustomHopArrayAdapter(Context context, int textViewResourceId, ArrayList<HopInfo> hopItems){
        super(context, textViewResourceId, hopItems);
        this.appContext = context;
        this.hopItems = hopItems;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) appContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.hop_list_items, null);
        }
        HopInfo hops = hopItems.get(position);
        if (hops != null) {
            //Get value fields from the View
            TextView hopAddition = (TextView) v.findViewById(R.id.hopAddition);
            TextView hopWeight = (TextView) v.findViewById(R.id.hopWeight);
            TextView hopTime = (TextView) v.findViewById(R.id.gallonTxt);
            Button btnDelete = (Button) v.findViewById(R.id.lst_item_Delete);
            btnDelete.setTag(position);

            //When the delete button is pressed
            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Get the current position
                    String pos = view.getTag().toString();
                    int remPos = Integer.parseInt(pos);
                    //Remove the position and notify the View that a change has occured
                    hopItems.remove(remPos);
                    notifyDataSetChanged();
                }
            });

            //Get values from the found hop object and use it to add to the
            //list view object
            if (hopAddition != null){
                hopAddition.setText(hops.getName());
            }
            if (hopWeight != null){
                hopWeight.setText(hops.getWeight());
            }
            if(hopTime != null){
                hopTime.setText(hops.getTime());
            }
        }
        return v;
    }
}

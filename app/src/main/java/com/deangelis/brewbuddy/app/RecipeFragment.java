package com.deangelis.brewbuddy.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Chris on 6/7/2014.
 *
 * CLASS: RecipieFragment
 *
 * Fragment designed to allow the user to describe the ingredients
 * to be utilized in the brew and return approximate ABV, IBUs, and
 * Color.
 */

public class RecipeFragment extends Fragment {
    View rootView;
    Spinner grain1,grain2,grain3,grain4,grain5,grain6;
    Grain Grain1;
    String sGrain1,sGrain2,sGrain3,sGrain4,sGrain5,sGrain6;
    EditText weight1, weight2, weight3, weight4, weight5, weight6;
    TextView abvText;
    EditText volume;
    ColorLUT LUT = new ColorLUT();
    float color1;
    Button addHopButton;
    CustomHopArrayAdapter hopAdapter = null;
    ArrayList<HopInfo> newHopList = null;
    ListView hopList;
    public static final int RESULT_OK = 201;

    private static final String ARG_SECTION_NUMBER = "section_number";

    public static RecipeFragment newInstance(int sectionNumber) {
        RecipeFragment fragment = new RecipeFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }
    public RecipeFragment() {
        // Required empty public constructor
    }

    /*
     * Initalized the layout view and variables upon creation
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_recipe, container, false);

        //Set all of the Fragment Local variables to their layout IDs
        weight1 = (EditText) rootView.findViewById(R.id.grainWeight1);
        weight2 = (EditText) rootView.findViewById(R.id.grainWeight2);
        weight3 = (EditText) rootView.findViewById(R.id.grainWeight3);
        weight4 = (EditText) rootView.findViewById(R.id.grainWeight4);
        weight5 = (EditText) rootView.findViewById(R.id.grainWeight5);
        weight6 = (EditText) rootView.findViewById(R.id.grainWeight6);
        volume = (EditText) rootView.findViewById(R.id.batchVolume);
        grain1 = (Spinner) rootView.findViewById(R.id.grain1Selection);

        abvText = (TextView) rootView.findViewById(R.id.abvValue);
        newHopList = new ArrayList<HopInfo>();
        hopAdapter = new CustomHopArrayAdapter(getActivity(), R.layout.hop_list_items, newHopList);

        hopList = (ListView) rootView.findViewById(R.id.hopListView);
        hopList.setAdapter(hopAdapter);


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.types_of_grains, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        grain1.setAdapter(adapter);
        grain1.setOnItemSelectedListener(new OnItemSelectedListener() {
             @Override
             public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Connect to SQL database and get Grain values
                //Grain1 =
             }

             @Override
             public void onNothingSelected(AdapterView<?> parent) {

             }
         });

        textChangeMethod();

        addHopButton = (Button) rootView.findViewById(R.id.addHopButton);
        addHopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showHopDialog();
            }
        });
        return rootView;
    }

    /*
     * Creates Text Change Listener objects for TextEdit fields
     * in the Recipe Fragment
     */
    public void textChangeMethod(){

        //Create new TextWatcher object
        TextWatcher filterTextWatcher = new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count,int after)
            {
            }

            public void onTextChanged(CharSequence s,int start, int before,int count)
            {
                calculateBrew();
            }

            @Override
            public void afterTextChanged(Editable arg0)
            {
            }
        };

        //Add listeners to the EditText fields
        weight1.addTextChangedListener(filterTextWatcher);
        volume.addTextChangedListener(filterTextWatcher);
        //weight2.addTextChangedListener(filterTextWatcher);
        //weight3.addTextChangedListener(filterTextWatcher);
        //weight4.addTextChangedListener(filterTextWatcher);
        //weight5.addTextChangedListener(filterTextWatcher);
        //weight6.addTextChangedListener(filterTextWatcher);
    }

    public void calculateBrew(){
        String sWeight1 = weight1.getText().toString();
        String sVolume = volume.getText().toString();
        //When both fields are not empty
        if( !(sVolume.equals("")) && !(sWeight1.equals("")) )  {
            //Get the field contents
            float fWeight1 = Float.parseFloat(sWeight1);
            float fVolume = Float.parseFloat(sVolume);
            //Calculate brew color
            //float MCU1 = fWeight1*(Grain1.getColor())/fVolume;
            //float MCU = MCU1;
            //float SRM = (float) (1.4922*(MCU*0.6859));

            //int[] newColor = LUT.getColor(SRM);
        }
    }

    public void showHopDialog(){
        getActivity().getSupportFragmentManager();
        new HopPopup();
        HopPopup newHop = HopPopup.newInstance();
        newHop.setTargetFragment(this, 201);
        newHop.show(getFragmentManager(), "fragmentDialog");
    }

    //Retrieve Intent objects from the Popup Windows that can be called from this Fragment
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Retrieve data from the set time popup window
        if (requestCode == RESULT_OK) {

            String hop = data.getStringExtra("hop_res");
            String time = data.getStringExtra("time_res");
            String weight = data.getStringExtra("weight_res");

            newHopList = new ArrayList<HopInfo>();
            HopInfo newInfo = new HopInfo();
            newInfo.setName(hop);
            newInfo.setTime(time);
            newInfo.setWeight(weight);
            newHopList.add(newInfo);

            //Add the newly created alarm to the CustomArrayAdapter
            if(newHopList != null && newHopList.size() >0){
                hopAdapter.notifyDataSetChanged();
                hopAdapter.add(newHopList.get(0));
            }
            //Notify the adapter that changes were made
            hopAdapter.notifyDataSetChanged();
        }
    }

}
